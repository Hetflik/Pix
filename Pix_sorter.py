#from multiprocessing import Process
from pathlib import Path
from collections import deque
import numpy as np
import matplotlib.pyplot as plt
import h5py


def energy_convert(index, ToT):
    #print(index)
    with h5py.File('Calibration.h5', 'r') as hf:
        data = hf['.']['Calibration']
        coeffs = data.value[index-1]
    return coeffs[0]*ToT+coeffs[1]

def write_bin(All_pix_array,index,ToT):
    a = All_pix_array[index,ToT]
    a += 1
    All_pix_array[index,ToT] = a
    return All_pix_array

def one_pixel_sorter(m,All_pix_array):
    url = "e:/2017-10-NewData/170822_Calib_TPX3_FeAm/ToT_ToA_calib_FeAm_r%s.t3pa" %(m)
    check_range = 15
    skip_rows = []
    pd_data = deque([], maxlen=check_range)

    with open(url, buffering = 1024*1024*1000) as infile:
        next(infile)
        for _ in range(check_range):
            line = infile.readline()
            pd_data.append([int(n) for n in line.split()])
            print(len(pd_data))
        for line in infile:
            main_index = pd_data[0][0]
            if main_index not in skip_rows:
                more_pixels = 0
                matrix_index = pd_data[0][1]
                index_all = index_around(matrix_index)
                for i in range(1,check_range):
                    check_index = pd_data[i][1]
                    if check_index in index_all:
                        check_ToA = abs(pd_data[i][2]-pd_data[0][2])
                        if check_ToA < 5:
                            skip_rows.append(pd_data[i][0])
                            more_pixels = 1
                if more_pixels == 0:
                    write_bin(All_pix_array,matrix_index,pd_data[0][3])
                pd_data.append([int(n) for n in line.split()])
            else:
                skip_rows = [x for x in skip_rows if x != main_index]
                pd_data.append([int(n) for n in line.split()])
    return All_pix_array

def all_pixel_sorter(m):
    url = "e:/TPX3 - kalibrace - 170427/Fe/Fe_r%s.t3pa" %(m)
    check_range = 15
    skip_rows = []
    pd_data = deque([], maxlen=check_range)

    with open(url, buffering = 1024*1024*1000) as infile:
        next(infile)
        for _ in range(check_range):
            line = infile.readline()
            pd_data.append([int(n) for n in line.split()])
            print(len(pd_data))
        with open('Energies1.txt', 'a') as out:
            for line in infile:
                main_index = pd_data[0][0]
                if main_index not in skip_rows:
                    more_pixels = 0
                    matrix_index = pd_data[0][1]
                    index_all = index_around(matrix_index)
                    partial_energy = 0
                    for i in range(1,check_range):
                        check_index = pd_data[i][1]
                        if check_index in index_all:
                            check_ToA = abs(pd_data[i][2]-pd_data[0][2])
                            if check_ToA < 5:
                                partial_energy += energy_convert(matrix_index, pd_data[i][3])
                                skip_rows.append(pd_data[i][0])
                                more_pixels = 1 
                    if more_pixels == 0:
                        #energy = energy_convert(matrix_index, pd_data[0][3])
                        out.write(str(pd_data[0][3]) + '\n')
                        #out.write(str(energy) + '\n')
                    elif more_pixels == 1:
                        #print('More')
                        energy = partial_energy + energy_convert(matrix_index, pd_data[0][3])
                        out.write(str(energy) + '\n')
                    pd_data.append([int(n) for n in line.split()])
                else:
                    skip_rows = [x for x in skip_rows if x != main_index]
                    pd_data.append([int(n) for n in line.split()])

def index_around(matrix_index, rozmer=256):
    if matrix_index == 0:
        index_p = (matrix_index+1)
        index_sd = (matrix_index+rozmer)
        index_pd =  (matrix_index+rozmer+1)
        index_all = (matrix_index, index_p, index_sd, index_pd)
    if matrix_index == 255:
        index_l = (matrix_index-1)
        index_sd = (matrix_index+rozmer)
        index_ld =  (matrix_index+rozmer-1)
        index_all = (matrix_index, index_l, index_sd, index_ld)
    if matrix_index == 65280:
        index_p = (matrix_index+1)
        index_sh = (matrix_index-rozmer)
        index_ph =  (matrix_index-rozmer+1)
        index_all = (matrix_index, index_p, index_sh, index_ph)
    if matrix_index == 65535:
        index_l = (matrix_index-1)
        index_sh = (matrix_index-rozmer)
        index_lh =  (matrix_index-rozmer-1)
        index_all = (matrix_index, index_l, index_sh, index_lh)
    if 1 <= matrix_index <= 254:
        index_l = (matrix_index-1)
        index_p = (matrix_index+1)
        index_ld = (matrix_index+rozmer-1)
        index_sd = (matrix_index+rozmer)
        index_pd =  (matrix_index+rozmer+1)
        index_all = (matrix_index, index_l, index_p, index_ld, index_sd, index_pd)        
    if 65281 <= matrix_index <= 65534:
        index_lh = (matrix_index-rozmer-1)
        index_sh = (matrix_index-rozmer)
        index_ph = (matrix_index-rozmer+1)
        index_l = (matrix_index-1)
        index_p = (matrix_index+1)
        index_all = (matrix_index, index_lh, index_sh, index_ph, index_l, index_p)
    if matrix_index in range(256,65025,256):
        index_sh = (matrix_index-rozmer)
        index_ph = (matrix_index-rozmer+1)
        index_p = (matrix_index+1)
        index_sd = (matrix_index+rozmer)
        index_pd =  (matrix_index+rozmer+1)
        index_all = (matrix_index, index_sh, index_ph, index_p, index_sd, index_pd)
    if matrix_index in range(511,65280,256):
        index_lh = (matrix_index-rozmer-1)
        index_sh = (matrix_index-rozmer)
        index_l = (matrix_index-1)
        index_ld = (matrix_index+rozmer-1)
        index_sd = (matrix_index+rozmer)
        index_all = (matrix_index, index_lh, index_sh, index_l, index_ld, index_sd)
    else:
        index_lh = (matrix_index-rozmer-1)
        index_sh = (matrix_index-rozmer)
        index_ph = (matrix_index-rozmer+1)
        index_l = (matrix_index-1)
        index_p = (matrix_index+1)
        index_ld = (matrix_index+rozmer-1)
        index_sd = (matrix_index+rozmer)
        index_pd =  (matrix_index+rozmer+1)
        index_all = (matrix_index, index_lh, index_sh, index_ph, index_l, index_p, index_ld, index_sd, index_pd)
    return index_all

if __name__ == '__main__':
    MODE = 'ALL' #'ONE'
    if MODE == 'ONE':
        all = np.zeros((65536, 1025),dtype=np.int32)
        for i in range(1):
            print('Sorting file {}'.format(i))
            all = one_pixel_sorter(str(i).zfill(2), all)
            print(all[300])
        with h5py.File("Golem.h5", 'a') as out:
            out.create_dataset('Golem', data=all, compression='gzip')
        plt.figure(1)
        plt.plot(list(range(1,1025)), all[300], ls='steps')
        plt.figure(2)
        plt.bar(list(range(1,1025)), all[300])
        plt.show()
    elif MODE == 'ALL':
        for i in range(1):
            print('Sorting file {}'.format(i))
            all = all_pixel_sorter(str(i).zfill(2))