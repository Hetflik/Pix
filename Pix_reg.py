import h5py
import matplotlib.pyplot as plt
from scipy import stats
import numpy as np
from scipy.odr import ODR, Model, Data, RealData
from scipy.optimize import curve_fit

def linereg(x,*p):
    a, b = p
    return a*x+b

def linereg_odr(B, x):
    return B[0]*x+B[1]    

#model = Model(linereg_odr)

param=[]

for element in ('Zn_param', 'Se_param', 'Sr_param', 'Fe_param', 'Mn_param'):
    with h5py.File('{}.h5'.format(element), 'r') as hf:
        data = hf['.'][element].value
    param.append(data)
param = np.array(param)


plt.rc('font', family='serif')
coeffs = []
failed = []
for index in range(4939,4940):
    if index % 500 == 0:
        print('Fitting pixel {}'.format(index))
    params = (param[0, index, 0], param[1, index, 0], param[2, index, 0], param[2, index, 1], param[3, index, 0], param[4, index, 0])
    energ = (9.572, 12.4959, 15.8357, 14.165, 7.05798, 6.49045)
    #e = (param[0][2], param[1][2], param[2][2], param[2][3], param[3][2], param[4][2])
    e = (param[0, index, 2], param[1, index, 2], param[2, index, 2], param[2, index, 3], param[3, index, 2], param[4, index, 2])

    #params = (param[0][0], param[0][1], param[1][0],param[1][1], param[2][0],param[2][1],param[3][0],param[3][1],param[4][0],param[4][1])
    #energ = (9.572, 8.638, 12.495, 11.222, 15.835, 14.165, 7.058, 6.404, 6.49045, 5.89875)
    #e = (param[0][2],param[0][3],param[1][2],param[1][3],param[2][2],param[2][3],param[3][2],param[3][3],param[4][2],param[4][3])

    #mydata = RealData(energ, params, sy=e)
    #myodr = ODR(mydata, model, beta0=[1.,0.])
    #myoutput = myodr.run()
    #myoutput.pprint()
    #print(energ)


    try:
        coeff, var_matrix = curve_fit(linereg, energ, params, sigma=e, p0=[1,0], maxfev=100)
        errors = np.sqrt(np.diag(var_matrix))
        residu = linereg(np.array(energ), coeff[0], coeff[1])
        residua = ((params - residu)/e)
        a = np.append(np.append(coeff, errors), residua)
        coeffs.append(a)
    except RuntimeError:
        failed.append(index)
        coeffs.append([0,0,0,0,0,0,0,0,0,0])
        print('Fitting pixel {} failed'.format(index))

    y = linereg(np.arange(2,20,.01), coeff[0], coeff[1])

    f, axarr = plt.subplots(2, sharex=False, gridspec_kw = {'height_ratios':[4, 1]})
    plt.grid()
    axarr[0].set_ylabel('ToT [a.u.]')
    axarr[0].set_xlabel('E [keV]')
    axarr[0].plot(np.arange(2,20,.01), y, 'b')
    #y2 = linereg(np.arange(2,20,.01), myoutput.beta[0], myoutput.beta[1])
    #plt.plot(np.arange(2,20,.01), y2, 'r')
    axarr[0].errorbar(energ, params, yerr=e, fmt='.', color='navy', ecolor='black', capsize=3, elinewidth=1)
    axarr[1].set_xlim(0.5,6.5)
    axarr[1].set_ylim(-4,4)
    axarr[1].plot((1,2,3,4,5,6), residua, 'r.')
    #plt.figure(2)
    #plt.hist(residua, bins=6)
    #plt.xlim(-4,4)
    plt.show()

np.array(coeffs)
#with h5py.File("Calibration.h5", 'a') as out:
#    out.create_dataset('Calibration', data=coeffs, compression='gzip')
#with open('Failed_pixels_Calib2.txt', 'a') as out2:
#    out2.writelines(str(failed))

