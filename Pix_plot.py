import h5py
import matplotlib.pyplot as plt
import numpy as np
import re
from math import ceil


plt.rc('font', family='serif')
plt.rcParams.update({'mathtext.default':  'regular' })

def plot_points():
    param=[]

    for element in ('Zn_param', 'Se_param', 'Sr_param', 'Fe_param', 'Mn_param'):
        with h5py.File('{}.h5'.format(element), 'r') as hf:
            data = hf['.'][element].value
        param.append(data)
    param = np.array(param)

    with open('Failed_pixels_Calib_n2.txt') as failed:
        index = failed.read()
    index = [int(x) for x in re.sub("[,\[\]]", "", str(index)).split()]
    print(index)

    for i in index:
        params = (param[0, i, 0], param[1, i, 0], param[2, i, 0], param[2, i, 1], param[3, i, 0], param[4, i, 0])
        energ = (9.572, 12.4959, 15.8357, 14.165, 7.05798, 6.49045)
        e = (param[0, i, 2], param[1, i, 2], param[2, i, 2], param[2, i, 3], param[3, i, 2], param[4, i, 2])
        #print(param[0, index, 0])
        #print(energ)
        #plt.plot(params,energ, '.')
        plt.grid()
        plt.title(r'$Pixel \; {}$'.format(i))
        plt.ylabel('ToT [a.u.]')
        plt.xlabel('E [keV]')
        plt.errorbar(energ, params, yerr=e, fmt='.', color='navy', ecolor='black', capsize=3, elinewidth=1)
        plt.show()

def chessboard(calib_file):
    board = np.ones((256, 256))
    with open(calib_file) as failed:
        index = failed.read()
    index = [int(x) for x in re.sub("[,\[\]]", "", str(index)).split()]
    for pixel in index:
        řádek = ceil((pixel+1)/256)-1
        sloupec = pixel%256
        board[řádek,sloupec] = 0
    fig, ax = plt.subplots()
    plt.title('{}'.format(calib_file))
    plt.ylabel('index i [-]')
    plt.xlabel('index j [-]')
    plt.imshow(board, cmap=plt.cm.gray, interpolation='nearest')

#chessboard('Failed_pixels_Calib_n.txt')
chessboard('Failed_pixels_Calib_n2.txt')
#plot_points()

plt.show()
