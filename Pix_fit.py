import h5py
import math
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from astropy.convolution import convolve, Gaussian1DKernel
from detect_peaks import detect_peaks
from scipy.special import erf
from scipy.stats import chisquare

plt.rc('font', family='serif')
plt.rcParams.update({'mathtext.default':  'regular' })


def gauss_t(x, *p):
    A, mu, nu, sigma, B, Y = p
    return A*np.exp(-(x-mu)**2/(2.*sigma**2)) + A/2*np.exp(-(x-nu)**2/(2.*sigma**2)) + B/2*(Y+erf( (-x+((nu+2*mu)/3))/(np.sqrt(2*(sigma**2+(mu-nu)**2))) ))

def gauss_odr(B, x):
    return B[0]*np.exp(-(x-B[1])**2/(2.*B[3]**2)) + B[0]/2*np.exp(-(x-B[2])**2/(2.*B[3]**2)) + B[4]/2*(B[5]+erf( (-x+((B[2]+2*B[1])/3))/(np.sqrt(2*(B[3]**2+(B[1]-B[2])**2))) ))


#def gauss(x, *p):
#    A, mu, nu, sigma, alpha, beta, gamma, delta = p
#    return A*np.exp(-(x-mu)**2/(2.*sigma**2)) + A/2*np.exp(-(x-nu)**2/(2.*sigma**2)) + alpha*x**3 + beta*x**2 + gamma*x + delta

def polynom(x, *p):
    alpha, beta, gamma, delta = p
    return alpha*x**3 + beta*x**2 + gamma*x + delta

def errorfunc(x, *p):
    mu, nu, sigma, B, Y = p
    return B/2*(Y+erf( (-x+((nu+2*mu)/3))/(np.sqrt(2*(sigma**2+(mu-nu)**2))) ))

def line(x, *p):
    gamma, delta = p
    return gamma*x + delta

def one_gauss(x, *p):
    A, mu, sigma = p
    return A*np.exp(-(x-mu)**2/(2.*sigma**2))

def spectrum_plot(start, stop, peak, coeff, data, pixel):
    x = np.arange(start,stop,0.1)
    #hist_fit = gauss(np.arange(start,stop,0.1), *coeff)
    hist_fit = gauss_t(np.arange(start,stop,0.1), *coeff)
    hist_fit_one = one_gauss(x, coeff[0], coeff[1], coeff[3])
    hist_fit_two = one_gauss(x, coeff[0]/2, coeff[2], coeff[3])
    errorfunc_fit = errorfunc(x, coeff[1], coeff[2], coeff[3], coeff[4], coeff[5])
    plt.figure(1)
    plt.title(r'$Pixel \; {}$'.format(pixel),fontsize=20)
    plt.xlim(1, peak+45)
    plt.xlabel(r'$ToT \; [a.u.]$')
    plt.ylabel(r'#$OPE \; [-]$')
    plt.plot(np.arange(0.5,1024.5), data, ls='steps')
    plt.plot(np.arange(start,stop,0.1), hist_fit, 'r', label='Fitted data')
    plt.plot(x, hist_fit_one, 'green', label='Fitted data')
    plt.plot(x, hist_fit_two, 'orange', label='Fitted data')
    #plt.plot(x, polynom_fit, 'black', label='Fitted data')
    plt.plot(x, errorfunc_fit, 'black', label='Fitted data')
    plt.grid(True)
    plt.show()


def fit(start, stop, peak, data, pixel):
    hist_cut = data[start:stop]
    bin_centers_cut = np.arange(start,stop)
    p0 = [350, peak+3., peak-3., 3., 50, 1.] #Cr
    #coeff, var_matrix = curve_fit(gauss_t, bin_centers_cut, hist_cut, p0=p0, maxfev=1000000, bounds=([0,peak-4.,peak-7.,0.,0., 1.],[1500,peak+16.,peak+10,10.,1500, 500.]))
    coeff, var_matrix = curve_fit(gauss_t, bin_centers_cut, hist_cut, p0=p0, maxfev=1000000)
    #mydata = RealData(bin_centers_cut, hist_cut, sx=1, sy=50)
    #myodr = ODR(mydata, model, beta0=[400, 20., 19., 4., 100., 2.], maxit=1000000, sstol=0.0000000001, partol=0.000000001)
    #myoutput = myodr.run()
    #myoutput.pprint()
    #coeff, var_matrix = curve_fit(gauss_t, bin_centers_cut, hist_cut, p0=p0, maxfev=1000000, bounds=([0,peak-300.,peak-700.,0.,0., 1.],[1500,peak+600.,peak+100,50.,1500, 5000.]))
    errors = np.sqrt(np.diag(var_matrix))
    #print(coeff)
    #print(errors)
    #y2 = gauss_odr(myoutput.beta, np.arange(start,stop,0.1))
    #plt.plot(np.arange(start,stop,0.1), y2, 'navy')
    spectrum_plot(start, stop, peak, coeff, data, pixel)
    chi = chisquare(hist_cut, gauss_t(np.arange(start,stop,1),*coeff),ddof=6)
    #print(chi[0]/(len(hist_cut)-6), chi[1])
    return coeff[1], coeff[2], errors[1], errors[2], chi[0]/(len(hist_cut)-6), chi[1]

def main(element):
    fitted = np.zeros((65536, 6))
    failed_pixels = []

    with h5py.File('{}.h5'.format(element), 'r') as hf:
        if element == 'Sr':
            datas = hf['.']['{}_gzip'.format(element)].value
        else:
            datas = hf['.']['{}'.format(element)].value

    for pixel in range(15235,15250):
        data = datas[pixel]
        if pixel % 500 ==0:
            print('Fitting pixel {}'.format(pixel))
        _ = 0
        smooth_factor = 1
        while _ == 0:
            kerG = Gaussian1DKernel(smooth_factor)
            conG = convolve(data, kerG, boundary='extend')
            peaks = detect_peaks(conG, mph=0.001, mpd=10, show=False)
            if element == 'I':
                if len(peaks) == 1:
                    a, b, c, d, e, f = fit(int(peaks[0]-peaks[0]/5.5), int(peaks[0]+peaks[0]/1.5),peaks[0], data, pixel)
                    cofs = [a, b, c, d, e, f]
                    fitted[pixel] = cofs
                    _ = 1
                elif smooth_factor > 20 and len(peaks) < 4 and len(peaks) > 0:
                    a, b, c, d, e, f = fit(int(peaks[-1]-peaks[-1]/5.5), int(peaks[-1]+peaks[-1]/1.5),peaks[0], data, pixel)
                    cofs = [a, b, c, d, e, f]
                    fitted[pixel] = cofs
                    _ = 1
                elif smooth_factor > 300:
                    print('Pixel {} failed'.format(pixel))
                    failed_pixels.append(pixel)
                    peaks = detect_peaks(conG, mph=0.01, mpd=10, show=True)
                    break
                else:
                    smooth_factor += 1

            elif element == 'Mn':
                if len(peaks) == 1:
                    try:
                        a, b, c, d, e, f = fit(int(peaks[0]-peaks[0]/1.1), int(peaks[0]+peaks[0]/0.8),peaks[0], data, pixel)
                        cofs = [a, b, c, d, e, f]
                        fitted[pixel] = cofs
                    except:
                        fitted[pixel] = [0,0,0,0,0,0]
                        print('Pixel {} failed'.format(pixel))
                        failed_pixels.append(pixel)
                    _ = 1
                elif smooth_factor > 10 and len(peaks) < 4 and len(peaks) > 0:
                    try:
                        a, b, c, d, e, f = fit(int(peaks[0]-peaks[0]/1.1), int(peaks[0]+peaks[0]/0.8),peaks[0], data, pixel)
                        cofs = [a, b, c, d, e, f]
                        fitted[pixel] = cofs
                    except:
                        fitted[pixel] = [0,0,0,0,0,0]
                        print('Pixel {} failed'.format(pixel))
                        failed_pixels.append(pixel)
                    _ = 1
                elif smooth_factor > 250:
                    print('Pixel {} failed'.format(pixel))
                    failed_pixels.append(pixel)
                    peaks = detect_peaks(conG, mph=0.01, mpd=10, show=False)
                    fitted[pixel] = [0,0,0,0,0,0]
                    break
                else:
                    smooth_factor += 1

            elif element == 'Se':
                if len(peaks) == 1:
                    try:
                        a, b, c, d, e, f = fit(int(peaks[0]-peaks[0]/2.4), int(peaks[0]+peaks[0]/1.8),peaks[0], data, pixel)
                        cofs = [a, b, c, d, e, f]
                        fitted[pixel] = cofs
                    except:
                        fitted[pixel] = [0,0,0,0,0,0]
                        print('Pixel {} failed'.format(pixel))
                        failed_pixels.append(pixel)
                    _ = 1
                elif smooth_factor > 10 and len(peaks) < 4 and len(peaks) > 0:
                    try:
                        a, b, c, d, e, f = fit(int(peaks[0]-peaks[0]/2.4), int(peaks[0]+peaks[0]/1.8),peaks[0], data, pixel)
                        cofs = [a, b, c, d, e, f]
                        fitted[pixel] = cofs
                    except:
                        fitted[pixel] = [0,0,0,0,0,0]
                        print('Pixel {} failed'.format(pixel))
                        failed_pixels.append(pixel)
                    _ = 1
                elif smooth_factor > 250:
                    print('Pixel {} failed'.format(pixel))
                    failed_pixels.append(pixel)
                    peaks = detect_peaks(conG, mph=0.01, mpd=10, show=False)
                    fitted[pixel] = [0,0,0,0,0,0]
                    break
                else:
                    smooth_factor += 1

            elif element == 'Fe':
                if len(peaks) == 1:
                    try:
                        a, b, c, d, e, f = fit(int(peaks[0]-peaks[0]/1.), int(peaks[0]+peaks[0]/1.),peaks[0], data, pixel)
                        cofs = [a, b, c, d, e, f]
                        fitted[pixel] = cofs
                    except:
                        fitted[pixel] = [0,0,0,0,0,0]
                        print('Pixel {} failed'.format(pixel))
                        failed_pixels.append(pixel)
                    _ = 1
                elif smooth_factor > 10 and len(peaks) < 4 and len(peaks) > 0:
                    try:
                        a, b, c, d, e, f = fit(int(peaks[0]-peaks[0]/1.), int(peaks[0]+peaks[0]/1.),peaks[0], data, pixel)
                        cofs = [a, b, c, d, e, f]
                        fitted[pixel] = cofs
                    except:
                        fitted[pixel] = [0,0,0,0,0,0]
                        print('Pixel {} failed'.format(pixel))
                        failed_pixels.append(pixel)
                    _ = 1
                elif smooth_factor > 250:
                    print('Pixel {} failed'.format(pixel))
                    failed_pixels.append(pixel)
                    peaks = detect_peaks(conG, mph=0.01, mpd=10, show=False)
                    fitted[pixel] = [0,0,0,0,0,0]
                    break
                else:
                    smooth_factor += 1

            elif element == 'Zn':
                if len(peaks) == 1:
                    try:
                        a, b, c, d, e, f = fit(int(peaks[0]-peaks[0]/1.8), int(peaks[0]+peaks[0]/1.5),peaks[0], data, pixel)
                        cofs = [a, b, c, d, e, f]
                        fitted[pixel] = cofs
                    except:
                        fitted[pixel] = [0,0,0,0,0,0]
                        print('Pixel {} failed'.format(pixel))
                        failed_pixels.append(pixel)
                    _ = 1
                elif smooth_factor > 10 and len(peaks) < 4 and len(peaks) > 0:
                    try:
                        a, b, c, d, e, f = fit(int(peaks[0]-peaks[0]/1.8), int(peaks[0]+peaks[0]/1.5),peaks[0], data, pixel)
                        cofs = [a, b, c, d, e, f]
                        fitted[pixel] = cofs
                    except:
                        fitted[pixel] = [0,0,0,0,0,0]
                        print('Pixel {} failed'.format(pixel))
                        failed_pixels.append(pixel)
                    _ = 1
                elif smooth_factor > 250:
                    print('Pixel {} failed'.format(pixel))
                    failed_pixels.append(pixel)
                    peaks = detect_peaks(conG, mph=0.01, mpd=10, show=False)
                    fitted[pixel] = [0,0,0,0,0,0]
                    break
                else:
                    smooth_factor += 1

            elif element == 'Sr':
                if len(peaks) == 1:
                    try:
                        a, b, c, d, e, f = fit(int(peaks[0]-peaks[0]/2.), int(peaks[0]+peaks[0]/2.),peaks[0], data, pixel)
                        cofs = [a, b, c, d, e, f]
                        fitted[pixel] = cofs
                    except:
                        fitted[pixel] = [0,0,0,0,0,0]
                        print('Pixel {} failed'.format(pixel))
                        failed_pixels.append(pixel)
                    _ = 1
                elif smooth_factor > 10 and len(peaks) < 4 and len(peaks) > 0:
                    try:
                        a, b, c, d, e, f = fit(int(peaks[0]-peaks[0]/2.), int(peaks[0]+peaks[0]/2.),peaks[0], data, pixel)
                        cofs = [a, b, c, d, e, f]
                        fitted[pixel] = cofs
                    except:
                        fitted[pixel] = [0,0,0,0,0,0]
                        print('Pixel {} failed'.format(pixel))
                        failed_pixels.append(pixel)
                    _ = 1
                elif smooth_factor > 250:
                    print('Pixel {} failed'.format(pixel))
                    failed_pixels.append(pixel)
                    peaks = detect_peaks(conG, mph=0.01, mpd=10, show=False)
                    fitted[pixel] = [0,0,0,0,0,0]
                    break
                else:
                    smooth_factor += 1        
            else:
                break

    #with h5py.File("{}_param.h5".format(element), 'a') as out:
    #    out.create_dataset('{}_param'.format(element), data=fitted, compression='gzip')
    #with open('Failed_pixels {}.txt'.format(element), 'a') as out2:
    #    out2.writelines(str(failed_pixels))


main('I')
